import { useState, useEffect, useContext } from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login(){

    const {user, setUser} = useContext(UserContext);
    
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false);

    function authenticate(e){
        e.preventDefault();

        //LOGIN AUTHENTICATION
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: 'Login Successful!',
                    icon: 'success',
                    text: 'Welcome to Zytech!'
                })
            } else {
                Swal.fire({
                    title: 'Authentication Failed',
                    icon: 'error',
                    text: 'Check your login details and try again.'
                })
            }
        });

        setEmail('');
        setPassword('');
        
    }

    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/:userId/userDetails`, {
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }


    useEffect(() => {
        if(email !== '' && password !==''){
            setIsActive(true);
        } else{
            setIsActive(false);
        }
    }, [email, password])

    return (
        (user.id !== null) ?
        <Navigate to ="/products" />
        :
        <Form onSubmit={(e) => authenticate(e)}>
            <h4 className="title-header">L O G I N</h4>
            <p className="subtitle-header">Please enter your email and password:</p>
            <Form.Group className="login-input" controlId="userEmail" >
                <Form.Control 
                    className="input-height"
                    type="email" 
                    placeholder="Email" 
                    required 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
            </Form.Group>

            <Form.Group className="login-input" controlId="password1">
                <Form.Control 
                    className="input-height"
                    type="password" 
                    placeholder="Password" 
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
            </Form.Group>

            { isActive ?
                <Button className="login-button" variant="outline-primary" type="submit" id="submitBtn">LOGIN</Button>
                :
                <Button className="login-button" variant="outline-primary" type="submit" id="submitBtn" disabled>LOGIN</Button>
            }
             
            <p className="subtitle-header">Don't have an account? <Link to="/register">Register Here</Link></p>
        </Form>
    )
}
