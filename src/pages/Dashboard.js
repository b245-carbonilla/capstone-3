import { useEffect, useState, useContext } from "react";
import { Button, Table, Modal } from "react-bootstrap";
import { Navigate, Link } from "react-router-dom";

import ProductAdd from "../components/ProductAdd.js";
import UserContext from "../UserContext.js";
import Swal from "sweetalert2";

export default function AdminDashboard(){

	const { user } = useContext(UserContext);

	const [showAdd, setShowAdd] = useState(false);
	//const [showEdit, setShowEdit] = useState(false);

	const handleCloseAdd = () => setShowAdd(false);
	const handleShowAdd = () => setShowAdd(true);

	const [allProducts, setAllProducts] = useState([]);

	const fetchProducts = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td className="d-flex flex-column">
							{
								(product.isActive)
								?	
								<Button className="w-100 my-1" variant="danger" size="sm" onClick ={() => archive(product._id, product.name)}>
									Archive
								</Button>
								:
								<>
									<Button className="w-100 my-1" variant="primary" size="sm" onClick ={() => unarchive(product._id, product.name)}>
										Activate
									</Button>
								</>
							}
							<Button as={ Link } to={`/products/${product._id}/edit`} variant="secondary" size="sm" className="my-1 w-100" >Edit</Button>
						</td>
					</tr>
				)
			}))
		})
	}

	const archive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Archived Succesfully!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchProducts();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong!, Please try again later`
				})
			}
		})
	}

	const unarchive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Unarchived Succesfully!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchProducts();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Sorry, Something went wrong!`
				})
			}
		})
	}

	useEffect(()=>{

		fetchProducts();
	},[])


	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-4 mb-5 text-center">
				<h1>ADMIN DASHBOARD</h1>
				<Button onClick={handleShowAdd} variant="primary" size="lg" className="mx-2" id="btnAdd">
					Add Product
				</Button>
			
			</div>
			<Table striped bordered hover id="table">
		     <thead className="py-5 my-5">
		       <tr>
		         <th>Name</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       {allProducts}
		     </tbody>
		   </Table>

		<Modal show={showAdd} onHide={handleCloseAdd}>
			<ProductAdd closeAddProp={handleCloseAdd}/>;
		</Modal>
		</>
		:
		<Navigate to="/products"/>

	)
}
