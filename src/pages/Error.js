import Banner from '../components/Banner';

export default function Error() {
	const data = {
		title : "Error 404 - Not Found",
		content : "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to home"
	}
	return (
		<Banner data={data}/>
	)
}
