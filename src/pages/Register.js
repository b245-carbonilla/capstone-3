import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Form, Button } from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';

export default function Register() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    
    const {user, setUser} = useContext(UserContext);
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate()
    
    function registerUser(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if (data) {

                Swal.fire({
                    title: "Duplicate email found!",
                    icon: "error",
                    text: "Kindly provide another email to complete the registration."
                })
            } else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        mobileNo: mobileNo,
                        email: email,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if(data) {
                        Swal.fire({
                            title: "Registration Successful!",
                            icon: "success",
                            text: "Welcome to Zytech!"
                        })
                              
                        navigate('/login')

                        // Clear input fields
                        setFirstName('');
                        setLastName('');
                        setMobileNo('');
                        setEmail('');
                        setPassword1('');
                        setPassword2('');

                    } else {
                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        })
                    }
                     
                })
            }
        })
    }

    useEffect(() => {
        if((firstName !== '' && 
            lastName !== '' && 
            mobileNo !== '' && 
            email !== '' && 
            password1 !== '' && 
            password2 !== '') && (
            password1 === password2) && (
            mobileNo.length >= 11)
            ){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    
    }, [firstName, lastName, mobileNo, email, password1, password2]);

    return (
        (user.id !== null) ? 
        <Navigate to ="/products" />
        :
        <Form onSubmit={(e) => registerUser(e)}>
            <h4 className="title-header">R E G I S T E R</h4>
            <p className="subtitle-header">Please fill in the information below:</p>
            <Form.Group  className="login-input" controlId="firstName">
                <Form.Control
                    className="input-height" 
                    type="text" 
                    placeholder="First name" 
                    required 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                />
            </Form.Group>
            <Form.Group  className="login-input" controlId="lastName">
                <Form.Control 
                    className="input-height" 
                    type="text" 
                    placeholder="Last name" 
                    required 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                />
            </Form.Group>
            <Form.Group  className="login-input" controlId="mobileNo">
                <Form.Control 
                    className="input-height" 
                    type="text" 
                    placeholder="Mobile number (11 digits)" 
                    required 
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                />
            </Form.Group>
            <Form.Group className="login-input" controlId="userEmail">
                <Form.Control 
                    className="input-height" 
                    type="email" 
                    placeholder="Email" 
                    required 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
            </Form.Group>

            <Form.Group  className="login-input" controlId="password">
                <Form.Control 
                    className="input-height" 
                    type="password" 
                    placeholder="Password" 
                    required
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                />
            </Form.Group>

            <Form.Group  className="login-input" controlId="password">
                <Form.Control 
                    className="input-height" 
                    type="password" 
                    placeholder="Verify password" 
                    required
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                />
            </Form.Group>

            { isActive ?
                <Button className="register-button" variant="outline-primary" type="submit" id="submitBtn">CREATE MY ACCOUNT</Button>
                :
                <Button className="register-button" variant="outline-primary" type="submit" id="submitBtn" disabled>CREATE MY ACCOUNT</Button>
            }
        </Form>
    )
}

