import Banner from '../components/Banner';
import { Container } from 'react-bootstrap';
import Highlights from '../components/Highlights';

export default function Home() {

	const data = {
		title : "ZYTECH COMPUTER HUB",
		content : "Computer Parts & Services",
		destination: "/products",
		label: "Order Now!"
	}
	return (
		<>
			
            <Container>
	          <Banner data={data}/> 
	          <Highlights/>         
        	</Container>
        </>
	)
}
