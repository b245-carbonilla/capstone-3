import './App.css';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { UserProvider } from './UserContext';


// IMPORT PAGES
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Dashboard from './pages/Dashboard';
import Products from './pages/Products';


// IMPORT COMPONENTS
import AppNavbar from './components/AppNavbar';
//import AppFooter from './components/AppFooter';

import ProductAdd from './components/ProductAdd';
import ProductEdit from './components/ProductEdit';
import ProductView from './components/ProductView';


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }


useEffect(() => {
  console.log(user);
  console.log(localStorage);
}, [user])

return (
    
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
            <Routes>
              <Route path="/" element={<Home/>} />
              <Route path="/register" element={<Register/>} />
              <Route path="/login" element={<Login/>} />  
              <Route path="/logout" element={<Logout/>} />
              <Route path="/products" element={<Products/>} />
              <Route path="/products/:productId" element={<ProductView/>} />
              <Route path="/admin" element={<Dashboard/>} />
              <Route path="/addproduct" element={<ProductAdd/>} />
              <Route path="/products/:productId/edit" element={<ProductEdit/>}/>

              <Route path="*" element={<Error/>} />  
          </Routes>
        </Container>
        {/*<AppFooter />*/}
      </Router>
    </UserProvider>
    
  );
}

export default App;
