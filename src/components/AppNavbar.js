import {useContext} from 'react';
import {Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

export default function AppNavbar() {

  // State to store the user information stored in the login page.
  // const [user, setUser] = useState(localStorage.getItem('email'));

  const { user } = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/">ZYTECH</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link as={NavLink} to="/">HOME</Nav.Link>

            { (user.isAdmin) ?
              <Nav.Link as={Link} to="/admin">ADMIN</Nav.Link>
              :
              <Nav.Link as={NavLink} to="/products">PRODUCTS</Nav.Link>
            }
            
            { (user.id !== null) ?
              <Nav.Link as={NavLink} to="/logout">LOGOUT</Nav.Link>
              :
              <>
              <Nav.Link as={NavLink} to="/register">REGISTER</Nav.Link>
              <Nav.Link as={NavLink} to="/login">LOGIN</Nav.Link>
              </>
            } 
          </Nav>
        </Navbar.Collapse>
    </Navbar>
  );
}

