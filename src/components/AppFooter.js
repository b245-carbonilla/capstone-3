import '../style/styles.css';

export default function AppFooter() {
    return (
        <footer className="footer">&copy; 2023 ZYTECH - All Rights Reserved</footer>
    );
}