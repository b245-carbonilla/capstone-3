import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights() {
	return (
	   <div className="highlights-container">
            <Row className="mt-3 mb-3 featured-container">
            <h1 className="title-header">FEATURED PRODUCTS</h1>
                <Col xs={12} md={4}>
                    <Card className="highlights-card border-0">
                        <Card.Img variant="top" src={require('../images/msi-crosshair.jpg')} />
                        <Card.Body>
                            <Card.Title>MSI Crosshair</Card.Title>
                            <Card.Text>
                                Experience fast and smooth gaming with MSI laptops.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={4}>
                    <Card className="highlights-card border-0">
                        <Card.Img variant="top" src={require('../images/msi-raider.jpg')} />
                        <Card.Body>
                            <Card.Title>MSI Raider</Card.Title>
                            <Card.Text>
                                Travel through time and space with ultimate performance and the irresistible design.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={4}>
                    <Card className="highlights-card border-0">
                        <Card.Img variant="top" src={require('../images/msi-titan.jpg')}/>
                        <Card.Body>
                            <Card.Title>MSI Titan</Card.Title>
                            <Card.Text>
                                Looking for lightweight and high-end laptops?
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </div>
 
	)
}