import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { useNavigate } from "react-router-dom";

export default function ProductView() {
	const navigate = useNavigate()
	// The "useParams" hook allows us to retrieve the courseId passed via the URL
	const { productId } = useParams();

	// To be able to obtain the user ID so that we can enroll a user
	const { user } = useContext(UserContext);

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [totalAmount, setTotalAmount] = useState(0);


	useEffect(() => {
		console.log(productId);

		// Fetch request that will retrieve the details of the course from the database to be displayed in the "CourseView" page
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	// Function to enroll a user

	const purchase = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				quantity: quantity,
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true) {
				Swal.fire({
					title: "Successfully purchased",
					icon: "success",
					text: "You have successfully purchased this product!"
				})
				navigate('/products')
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}


	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Subtitle>Product Name</Card.Subtitle>
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text>
					        

					        {user.id !== null
					        	?
					        	<Button variant="primary" onClick={() => purchase(productId)}>Buy Now</Button>
					        	:
					        	<Button as={Link} to="/login" variant="danger">Log in to purchase</Button>
					        }

					    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
